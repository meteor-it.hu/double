import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageNotFoundComponent } from 'src/app/page-not-found/page-not-found.component';
import { DeckComponent } from '../deck/deck.component';

@NgModule({
	imports: [
		RouterModule.forRoot([
			{ path: 'deck', component: DeckComponent },
			{ path: '', redirectTo: 'deck', pathMatch: 'full' },
			{ path: '**', component: PageNotFoundComponent }
		]) 
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }

