import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CardService, Even } from '../card.service';

type formStyle = 'form-horizontal' | 'form-inline';

@Component({
	selector: 'app-deck-size-selector',
	templateUrl: './deck-size-selector.component.html'
})
export class DeckSizeSelectorComponent implements OnInit {

	@Input() styleClass: formStyle = 'form-inline';
	public deckForm: FormGroup = new FormGroup({});
	public even: Even;

	private formBuilder: FormBuilder = new FormBuilder();

	constructor(
		public cardService: CardService
	) {
		this.even = this.cardService.evens[0];
	}

	ngOnInit(): void {
		this.createForm();
	}

	public startGame(): void {
		this.cardService.deckSize.next(this.deckForm.value.deck);
	}

	private createForm() {
		this.deckForm = this.formBuilder.group({
			deck: [(this.even) ? this.even.value : this.cardService.evens[0], Validators.required]
		});
	}

}
