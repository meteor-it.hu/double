import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Even {
	id: number;
	value: 32;
}

interface Card {
	name: string;
	selected: boolean;
	disabled?: boolean;
}

interface Img {
	id: number;
	name: string;
}

@Injectable({
	providedIn: 'root'
})
export class CardService {

	public deckSize: BehaviorSubject<number> = new BehaviorSubject<number>(0);
	public cards: Card[] = [];
	public tryings: number = 0;
	public best: number = 0;
	public disabledAllCard: boolean = false;
	public evens: Even[] = [
		{ id: 1, value: 32 }
	];

	private images: Img[] = [
		{ id: 1, name: 'candy.png' },
		{ id: 2, name: 'daddy.png' },
		{ id: 3, name: 'danny.png' },
		{ id: 4, name: 'emily.png' },
		{ id: 5, name: 'fraddy.png' },
		{ id: 6, name: 'georgy.png' },
		{ id: 7, name: 'gerard.png' },
		{ id: 8, name: 'kylie.png' },
		{ id: 9, name: 'mammy.png' },
		{ id: 10, name: 'pedro.png' },
		{ id: 10, name: 'peppa.png' },
		{ id: 10, name: 'rebeca.png' },
		{ id: 10, name: 'richard.png' },
		{ id: 10, name: 'suzy.png' },
		{ id: 10, name: 'wandy.png' },
		{ id: 10, name: 'zoe.png' }
	];

	constructor() {
		this.deckSize.subscribe(size => {
			this.setCard(size);
		});
	}

	public restart(): void {
		this.deckSize.next(this.deckSize.value);
		this.tryings = 0;
	}

	public selectCard(card: Card): void {
		if (this.disabledAllCard) {
			return;
		}
		++this.tryings;

		const sameCard = this.findSelectedCard(card);
		if (sameCard) {
			card.disabled = true;
			card.selected = false;
			sameCard.disabled = true;
			sameCard.selected = false;
			this.checkIfAllDisabled();
		} else {
			const selectedCards = this.cards.filter(card => card.selected === true);
			card.selected = true;
			if (selectedCards.length > 0) {
				this.turnBack(card);
			}
		}
	}

	private checkIfAllDisabled(): void {
		const disabledCards = this.cards.filter(card => card.disabled === true).length;
		if(disabledCards === this.cards.length) {
			this.best = this.tryings;
		}
	}

	private async turnBack(card: Card): Promise<void> {
		this.disabledAllCard = true;
		await this.wait();
		this.cards.map(card => card.selected = false);
		card.selected = false;
		this.disabledAllCard = false;
	}

	private wait(): Promise<any> {
		return new Promise(resolve => setTimeout(resolve, 1000));
	}

	private findSelectedCard(currentCard: Card): Card {
		return this.cards.find(card => card.selected === true && card.name === currentCard.name && card !== currentCard);
	}

	private setCard(size: number): void {
		this.cards.length = 0;
		for (let i = 0; i < (size / 2); ++i) {
			this.addCard(i);
			this.addCard(i);
		}
		this.shuffle();
	}

	private addCard(i: number) {
		this.cards.push({ name: this.images[i].name, selected: false });
	}

	private shuffle(): void {
		var currentIndex = this.cards.length, temporaryValue, randomIndex;
		while (0 !== currentIndex) {
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;
			temporaryValue = this.cards[currentIndex];
			this.cards[currentIndex] = this.cards[randomIndex];
			this.cards[randomIndex] = temporaryValue;
		}
	}

}
